# Personal Configs

A repository with standard config options like aliases for git at .bashrc and configs for .vimrc.
See [startup.md](startup.md) to read the instructions on how to build the environment.
