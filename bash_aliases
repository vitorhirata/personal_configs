# git alias
alias gst='git status'
alias gdf='git diff'

# vim color alias
alias vcl='vim ~/.vim/config/general/colors.vim'

# Master alias
alias ms='cd ~/Code/mestrado/main/src && tmux'
alias mcm='g++ main.cpp -std=c++11 -I lib/bmp -Ofast'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
#   sleep 10; bep
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias bep='play -n synth 0.5 sine 880 vol 0.4'

# Julia alias
alias jlp='julia --project=.'
