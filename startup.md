# Setup

## Basic programs install

### install applications available on package manager
blueman (bluetooth), vlc, telegram, properties-common (add utilities for apt such as apt-add-repository)
gimp, gparted, texmaker, pdftk.
```
sudo apt-get install blueman vlc telegram-desktop software-properties-common python3-launchpadlib gimp gparted texmaker pdftk
```

### install steam
first add contrib to the main line of /etc/apt/sources.list (first line). Then:
```
dpkg --add-architecture i386
sudo apt-get install steam-installer
```

### install discord
```
wget "https://discord.com/api/download?platform=linux&format=deb" -O discord.deb
sudo apt install ./discord.deb
```

### installing Banco do Brasil security module
```
wget https://cloud.gastecnologia.com.br/bb/downloads/ws/warsaw_setup64.deb
sudo dpkg -i warsaw_setup64.deb
```
to set in firefox go to in chrome chrome://settings/certificates search in Authorities for Warsaw Personal CA -> export.
Go to Firefox about:preferences#privacy -> authorities -> import (exported file)

### install zotero
```
wget -qO- https://raw.githubusercontent.com/retorquere/zotero-deb/master/install.sh | sudo bash
sudo apt update
sudo apt install zotero
```

### install spotify
```
curl -sS https://download.spotify.com/debian/pubkey_6224F9941A8AA6D1.gpg | sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update && sudo apt-get install spotify-client
```
### install zoom
```
sudo apt install libegl1-mesa libxcb-cursor0 libxcb-xtest0
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo apt install ./zoom_amd64.deb
rm zoom_amd64.deb
```

### firefox
install plugins: bitwarden, zotero. configure search bar. configure history clean on close


## Coding setup
### install tmux and tmate
```
sudo apt-get install tmux tmate
```

### install silver search
```
sudo apt install silversearcher-ag
```

### install vscode
```
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt install apt-transport-https
sudo apt update
sudo apt install code # or code-insiders
```

### install gephi
download from [gephi](https://gephi.org/users/download/)
```
tar xzvf gephi-0.10.1.tar.gz ## remember to change to the correct version
bash gephi-0.10.1/bin/gephi ## remember to change to the correct version
```

### installing python, virtualenv, pyenv
```
sudo apt install libsqlite3-dev bzip2 python3-tk tk-dev zlib1g-dev libffi-dev libssl-dev libbz2-dev libreadline-dev liblzma-dev
sudo apt install python3-venv python3-pip pipx
pipx ensurepath
pipx install virtualenv
curl https://pyenv.run | bash # install pyenv
```

### installing julia
```
curl -fsSL https://install.julialang.org | sh
```

### install vim
```
sudo apt-get install vim-gtk3
```

### add ssh and copy key to clipboard
```
ssh-keygen -t ed25519
sudo apt install xclip
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
This can be inserted in github and gitlab.


## Config files
### Clone personal_configs
```
mkdir ~/Code && cd ~/Code
git clone https://github.com/vitorhirata/personal_configs.git && cd personal_configs
```

### Configure vim
```
cp vimrc ~/.vimrc && cp -r vim ~/.vim
cd ~/.vim/bundle && sudo rm -r Vundle.vim/ && git clone https://github.com/VundleVim/Vundle.vim.git
```
open vim and type :PluginInstall

### Copy other configuration files
```
cp bashrc ~/.bashrc && cp profile ~/.profile && cp bash_alias ~/.bash_alias && cp tmux.conf ~/.tmux.conf && cp gitconfig ~/.gitconfig
```


## Optional installations
### install flatpak and firefox
```
sudo apt-get install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub org.mozilla.firefox
```

### instal xfce4
if in ubuntu; after install restart and on login choose xfce
```
sudo apt install xfce4
```

### install f.lux
```
sudo add-apt-repository ppa:nathan-renniewaldock/flux
sudo apt-get update
sudo apt-get install fluxgui
```

#### install mupen64 (nintendo64)
```
sudo apt-get install libsdl-ttf2.0-0 libsdl-ttf2.0-dev
```

### install haguici and hamachi (multiplayer gaming server)
```
wget https://vpn.net/installers/logmein-hamachi_2.1.0.203-1_amd64.deb
sudo dpkg -i logmein-hamachi_2.1.0.203-1_amd64.deb
sudo add-apt-repository -y ppa:webupd8team/haguichi
sudo apt update
sudo apt install -y haguichi
```

### install valgrind (c++ analyser)
```
sudo apt-get instal valgrind
```


## Extras (web dev deprecated)
### asdf (package manager)
See https://asdf-vm.com/guide/getting-started.html for more information and updata branch version.
```
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.14.0
echo -e '\n# set asdf environment variables' >> ~/.bashrc
echo -e '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc
echo -e '. "$HOME/.asdf/completions/asdf.bash"' >> ~/.bashrc
sudo apt install automake autoconf libreadline-dev libncurses-dev libssl-dev libyaml-dev libxslt-dev libffi-dev libtool unixodbc-dev unzip curl
```


### install ruby with asdf
In case there are problems, run `asdf reshim ruby {version installed}`
```
asdf plugin add ruby && asdf install ruby latest && asdf global ruby {version installed}
```

### install bundler version required (gem install bundler for simple install)
```
gem install bundler -v "$(grep -A 1 "BUNDLED WITH" Gemfile.lock | tail -n 1)"
bundle install
```

### configure postgres
```
sudo -u postgres psql
```
### rodar no terminal do postgres
no lugar de vitor deve estar o usuario do pc
```
create role vitor superuser; alter role vitor with login;
create role localuser with password '123' superuser; alter role local with login;
create role blumpa superuser; create role cloudsqlsuperuser superuser;
```

### configure database
```
rake db:setup
rake db:migrate
```

### install pdadmin4
```
sudo apt install postgresql-common
sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
sudo apt install pgadmin4 pgadmin4-apache2
```

### installing node using NVM (https://github.com/nvm-sh/nvm)
```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install node # install node latest version
```


## Troubleshooting

### Reduce brightness with xrand
```
xrandr --output eDP-1 --brightness 0.5
```

### Fix bluetooth not starting
```
sudo rmmod btusb && sleep 1 && sudo modprobe btusb
```

### Fix top panels not appearing
```
xfwm4 --replace
```

### Fix background not appearing
```
xfdesktop
```
