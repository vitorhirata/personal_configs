" Set background to dark. comment to set white theme, uncomment to set black theme
set background=dark

" Enable 256 colors in vim
set t_Co=256

" Colorscheme solarized
colorscheme solarized
