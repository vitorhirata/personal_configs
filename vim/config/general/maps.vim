" Define <leader>
let mapleader=','

" Disable arrows
map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

" Move to alternatefile, faster nem control+6
nnoremap <space><space> <c-^>

" Map Command+R to use find and replace
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" map ls to show buffer
nnoremap <leader>l :ls<cr>:b<space>
nnoremap <leader>ll :ls!<cr>:b<space>

" New line below with Alt+J and new line above with Alt+K
nnoremap <silent><A-j> :set paste<CR>m`o<Esc>``:set nopaste<CR>
nnoremap <silent><A-k> :set paste<CR>m`O<Esc>``:set nopaste<CR>

" Vim copy user clipboard
set clipboard=unnamedplus

" Fix common typos like :Q or :W
" (https://github.com/leafac/dotfiles/blob/master/vim/files/config/general/mappings.vim)
command! -bang -nargs=* -complete=file E e<bang> <args>
command! -bang -nargs=* -complete=file W w<bang> <args>
command! -bang -nargs=* -complete=file Wq wq<bang> <args>
command! -bang -nargs=* -complete=file WQ wq<bang> <args>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Q q<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>
